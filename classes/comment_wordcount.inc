<?php
/**
 * @file
 * This is the comment_wordcount class. It extends Wordcount, and is called by our comment_insert hook in the userpoints_wordcount module.
 * It uses variable_get/set to get stuff from our config form (# of words to award points to, the taxonomy to use, etc.
 * And it also pulls from the DB the body text and UID using the updated comment ID.
 */
 
class comment_wordcount extends wordcount
{

	private function grab_text($nid)
//                                  -grabs the text from the database, stores it in $this->text using the wordcount class's set function    
	{
			$this->set_text(db_query('SELECT comment_body_value FROM {field_data_comment_body} WHERE entity_id=:rid', array(':rid' => $nid))->fetchField());	
	}
	private function grab_uid($nid)
//                                  -grabs the userid from the database, stores it in $this->uid using the wordcount class's set function    
	{
			$this->set_uid((db_query('SELECT uid FROM {comment} WHERE nid=:rid', array(':rid' => $nid))->fetchField()));	
	}
	public function comment_wordcount($nid)
//                                  -this is the constructor. Pass it the comment id, it generates all the stuff it needs for the parent class's calc_wordcount function
	{
		$this->set_calc(variable_get('comment_wpc_use', true));
		if($this->check_calc())
		{
			$this->set_cat(variable_get('comment_wp_taxonomies', 0));
			$this->set_thresh(variable_get('comment_wpc_wordthreshold', 500));
			$this->set_points(variable_get('comment_wpc_pointperword', 1));
		
			$this->grab_text($nid);
			$this->grab_uid($nid);
		}		
	}
}