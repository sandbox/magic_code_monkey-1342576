<?php
/**
 * @file
 * This is a form generation class- since the wordcount config form got pretty heavy in the
 * size of the functions, I popped them in here and gave them their own class, to keep
 * from cluttering up the main module. 
 */
class wordcount_form
{
	public $form;
	private $taxonomy; //a list of the userpoint taxonomies
	public $wt; //weight
	
	private function get_taxonomy()
//  -this one gets all the userpoints taxonomies
	{
		$this->taxonomy=array();
	
		$up_tid=db_query("SELECT vid FROM {taxonomy_vocabulary} WHERE machine_name='userpoints'")->fetchField();
		$result=db_query('SELECT * FROM {taxonomy_term_data} WHERE vid = :vid', array(':vid' => $up_tid));
		$this->taxonomy[0]="General";
		foreach($result as $row)
		{
			$this->taxonomy[$row->tid]=$row->name;
		}
	}

	private function taxonomy_dropdown($node)
//  - creates a drop down based on the grabbed taxonomies    
	{
		
		$this->get_taxonomy();
		$this->form[$node . 'set'][$node . '_wp_taxonomies'] = array(
							'#type' => 'radios',
							'#title' => t('User Points Catagory to Use'),
							'#default_value' => variable_get($node . '_wp_taxonomies', 0),
							'#options' => $this->taxonomy,
							);
	}

	public function build_section($node)
	{
//  - This builds a single section, based on Node Type.  Node type is passed in, and it spits out what it needs. These are collapsable sets   
				$this->form[$node . 'set'] = array(
				      '#type' => 'fieldset', 
				      '#title' => t($node . ' Settings'), 
				      '#weight' => $this->wt, 
				      '#collapsible' => TRUE,
				    );
				    
				  $this->form[$node . 'set'][$node . '_wpc_use'] = array(
				    '#type' => 'checkbox',
				    '#title' => t('Make ' . $node . " use wordcount user points?"),
				    '#default_value' => variable_get($node . '_wpc_use', TRUE),
				  );
				  
				  $this->form[$node . 'set'][$node . '_wpc_wordthreshold'] = array(
				    '#type' => 'textfield',
				    '#title' => t('Number of words per point'),
				    '#default_value' => variable_get($node . '_wpc_wordthreshold', 500),
				    '#size' => 10,
				    '#maxlength' => 10,    
				    '#description' => t('The Number of words to award each point for'),
				    '#required' => TRUE,
				  );

				  $this->form[$node . 'set'][$node . '_wpc_pointperword'] = array(
				    '#type' => 'textfield',
				    '#title' => t('Number of points'),
				    '#default_value' => variable_get($node . '_wpc_pointperword', 1),
				    '#size' => 10,
				    '#maxlength' => 10,			    
				    '#description' => t('The number of points to award per threshold amount of words'),
				    '#required' => TRUE,
				  );
				$this->taxonomy_dropdown($node);
	}
	function wordcount_form($form) 
//  - create the form!  oooooh aaaaah    
	{
		$this->form=$form;
	}

}