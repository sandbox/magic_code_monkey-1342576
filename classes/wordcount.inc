<?php
/**
 * @file
 * This is the main wordcount class. It's a barebones way of calculating how many points
 * points to award for each post, and a way of awarding the points. It uses get/set
 * methods to do the voodoo it needs to do.
 *
 *This is never called directly, but instead used by a wrapper class that extends it and sets the right variables.
 */
class wordcount
{
	private $text;
	private $wordcount;
	private $wordthresh;
	private $points;
	private $uid;
	private $do_calc;
	private $cat;
	
	public function check_calc()
	{
		return $this->do_calc;
	}
	
	public function set_calc($calc)
	{
		$this->do_calc=$calc;
	}
	
	public function set_uid($id)
	{
		$this->uid=$id;
	}
	
	public function set_cat($id)
	{
		$this->cat=$id;
	}
	
	public function set_points($amt)
	{
		$this->points=$amt;
	}
	public function set_thresh($amt)
	{
		$this->wordthresh=$amt;
	}
	public function calc_point()
	{
		$x=(int)($this->wordcount/$this->wordthresh);        
		return $x * $this->points;
	}
	public function award_points()
	{
		if($this->do_calc)
		{
			$pt=$this->calc_point();
			$params = array();
			$params['uid'] = $this->uid;
			$params['points'] = $pt;
			$params['tid'] = $this->cat;
			userpoints_userpointsapi($params); 
		}
	}
	public function set_text($text)
	{
		$this->text=$text;
		$this->text=strip_tags($text);
		$this->wordcount=str_word_count($this->text);
	}
}